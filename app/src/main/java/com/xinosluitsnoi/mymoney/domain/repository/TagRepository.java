package com.xinosluitsnoi.mymoney.domain.repository;

import com.xinosluitsnoi.mymoney.domain.entity.Link;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface TagRepository {

    @Nullable
    Tag get(long id);

    @NonNull
    List<Tag> getAll();

    @NonNull
    List<Tag> getAll(@NonNull Link link);

    void add(@NonNull Tag tag);

    void update(@NonNull Tag tag);

    void delete(@NonNull Tag tag);
}