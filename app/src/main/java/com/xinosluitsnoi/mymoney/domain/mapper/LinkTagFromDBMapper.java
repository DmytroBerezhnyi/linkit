package com.xinosluitsnoi.mymoney.domain.mapper;

import com.xinosluitsnoi.mymoney.domain.database.entity.LinkTagCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.entity.LinkTag;

import androidx.annotation.NonNull;

public class LinkTagFromDBMapper implements BaseMapper<LinkTag, LinkTagCursorWrapper> {

    @NonNull
    @Override
    public LinkTag map(@NonNull LinkTagCursorWrapper linkTagCursorWrapper) {
        return new LinkTag(linkTagCursorWrapper.getLinkId(),
                           linkTagCursorWrapper.getTagId());
    }
}