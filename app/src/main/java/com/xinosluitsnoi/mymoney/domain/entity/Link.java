package com.xinosluitsnoi.mymoney.domain.entity;

import java.util.List;

import androidx.annotation.NonNull;

public class Link {

    private long id;

    private String link;

    private List<Tag> tags;

    private String description;

    private boolean isDone;

    private int rate;

    public Link(@NonNull String link,
                List<Tag> tags,
                @NonNull String description,
                boolean isDone,
                int rate) {
        this.link = link;
        this.tags = tags;
        this.description = description;
        this.isDone = isDone;
        this.rate = rate;
    }

    public Link(long id,
                @NonNull String link,
                List<Tag> tags,
                @NonNull String description,
                boolean isDone,
                int rate) {
        this.id = id;
        this.link = link;
        this.tags = tags;
        this.description = description;
        this.isDone = isDone;
        this.rate = rate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    @NonNull
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(@NonNull List<Tag> tags) {
        this.tags = tags;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}