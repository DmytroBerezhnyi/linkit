package com.xinosluitsnoi.mymoney.domain.database.entity;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor;

import androidx.annotation.NonNull;

public class LinkCursorWrapper extends CursorWrapper {

    private final int id;

    private final int link;

    private final int description;

    private final int isDone;

    private final int rate;

    public LinkCursorWrapper(@NonNull Cursor cursor) {
        super(cursor);

        id = getColumnIndex(DatabaseDescriptor.LinkEntity.Column._ID);
        link = getColumnIndex(DatabaseDescriptor.LinkEntity.Column.LINK);
        description = getColumnIndex(DatabaseDescriptor.LinkEntity.Column.DESCRIPTION);
        isDone = getColumnIndex(DatabaseDescriptor.LinkEntity.Column.IS_DONE);
        rate = getColumnIndex(DatabaseDescriptor.LinkEntity.Column.RATE);
    }

    public long getId() {
        return getLong(id);
    }

    @NonNull
    public String getLink() {
        return getString(link);
    }

    @NonNull
    public String getDescription() {
        return getString(description);
    }

    public boolean getIsDone() {
        return getInt(isDone) != 0;
    }

    public int getRate() {
        return getInt(rate);
    }
}