package com.xinosluitsnoi.mymoney.domain.mapper;

import android.content.ContentValues;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor.LinkTagEntity;
import com.xinosluitsnoi.mymoney.domain.entity.LinkTag;

import androidx.annotation.NonNull;

public class LinkTagToDBMapper implements BaseMapper<ContentValues, LinkTag> {

    @NonNull
    @Override
    public ContentValues map(@NonNull LinkTag linkTag) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(LinkTagEntity.Column.LINKS_ID, linkTag.getLinkId());
        contentValues.put(LinkTagEntity.Column.TAGS_ID, linkTag.getTagId());

        return contentValues;
    }
}