package com.xinosluitsnoi.mymoney.domain.database.entity;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor;

public class LinkTagCursorWrapper extends CursorWrapper {

    private final int linkId;

    private final int tagId;

    public LinkTagCursorWrapper(Cursor cursor) {
        super(cursor);

        linkId = getColumnIndex(DatabaseDescriptor.LinkTagEntity.Column.LINKS_ID);
        tagId = getColumnIndex(DatabaseDescriptor.LinkTagEntity.Column.TAGS_ID);
    }

    public long getLinkId() {
        return getLong(linkId);
    }

    public long getTagId() {
        return getLong(tagId);
    }
}
