package com.xinosluitsnoi.mymoney.domain.database.entity;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor;

import androidx.annotation.NonNull;

public class TagCursorWrapper extends CursorWrapper {

    private final int id;

    private final int tag;

    private final int rate;

    public TagCursorWrapper(@NonNull Cursor cursor) {
        super(cursor);

        id = getColumnIndex(DatabaseDescriptor.TagEntity.Column._ID);
        tag = getColumnIndex(DatabaseDescriptor.TagEntity.Column.TAG);
        rate = getColumnIndex(DatabaseDescriptor.TagEntity.Column.RATE);
    }

    public long getId() {
        return getLong(id);
    }

    @NonNull
    public String getTag() {
        return getString(tag);
    }

    public int getRate() {
        return getInt(rate);
    }
}