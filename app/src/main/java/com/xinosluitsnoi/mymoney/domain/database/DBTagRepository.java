package com.xinosluitsnoi.mymoney.domain.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.xinosluitsnoi.mymoney.domain.database.entity.LinkTagCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.database.entity.TagCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.entity.Link;
import com.xinosluitsnoi.mymoney.domain.entity.LinkTag;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;
import com.xinosluitsnoi.mymoney.domain.mapper.LinkTagFromDBMapper;
import com.xinosluitsnoi.mymoney.domain.mapper.TagFromDBMapper;
import com.xinosluitsnoi.mymoney.domain.mapper.TagToDBMapper;
import com.xinosluitsnoi.mymoney.domain.repository.TagRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DBTagRepository implements TagRepository {

    @NonNull
    private SQLiteDatabase database;

    @NonNull
    private final TagFromDBMapper tagFromDBMapper;

    @NonNull
    private final TagToDBMapper tagToDBMapper;

    @NonNull
    private final LinkTagFromDBMapper linkTagFromDBMapper;

    public DBTagRepository(@NonNull DBHelper dbHelper) {
        database = dbHelper.getReadableDatabase();

        tagFromDBMapper = new TagFromDBMapper();
        tagToDBMapper = new TagToDBMapper();
        linkTagFromDBMapper = new LinkTagFromDBMapper();
    }

    @Nullable
    @Override
    public Tag get(long id) {
        String getQuery = "SELECT * FROM " + DatabaseDescriptor.TagEntity.TABLE_NAME +
                " WHERE " + DatabaseDescriptor.TagEntity.Column._ID + " = " + id;

        try (Cursor cursor = database.rawQuery(getQuery, null);
             TagCursorWrapper tagCursorWrapper = new TagCursorWrapper(cursor)) {

            if (tagCursorWrapper.moveToFirst()) {
                return tagFromDBMapper.map(tagCursorWrapper);
            } else {
                return null;
            }
        }
    }

    @NonNull
    @Override
    public List<Tag> getAll() {
        List<Tag> tags = new ArrayList<>();
        String getAllQuery = "SELECT * FROM " + DatabaseDescriptor.TagEntity.TABLE_NAME;

        try (Cursor cursor = database.rawQuery(getAllQuery, null);
             TagCursorWrapper tagCursorWrapper = new TagCursorWrapper(cursor)) {

            if (tagCursorWrapper.moveToFirst()) {
                do {
                    tags.add(tagFromDBMapper.map(tagCursorWrapper));
                } while (tagCursorWrapper.moveToNext());
            }
        }
        return tags;
    }

    @NonNull
    @Override
    public List<Tag> getAll(@NonNull Link link) {
        List<Tag> tags = new ArrayList<>();
        String getAllQuery = "SELECT * FROM " + DatabaseDescriptor.LinkTagEntity.TABLE_NAME +
                " WHERE " + DatabaseDescriptor.LinkTagEntity.Column._ID + " = " + link.getId();

        try (Cursor cursor = database.rawQuery(getAllQuery, null);
             LinkTagCursorWrapper cursorWrapper = new LinkTagCursorWrapper(cursor)) {

            if (cursorWrapper.moveToFirst()) {
                LinkTag linkTag;
                do {
                    linkTag = linkTagFromDBMapper.map(cursorWrapper);
                    tags.add(get(linkTag.getTagId()));
                } while (cursorWrapper.moveToNext());
            }
        }
        return tags;
    }

    @Override
    public void add(@NonNull Tag tag) {
        ContentValues values = tagToDBMapper.map(tag);
        database.insert(DatabaseDescriptor.TagEntity.TABLE_NAME,
                        null,
                        values);
    }

    @Override
    public void update(@NonNull Tag tag) {
        ContentValues values = tagToDBMapper.map(tag);
        database.update(DatabaseDescriptor.TagEntity.TABLE_NAME,
                        values,
                        DatabaseDescriptor.TagEntity.Column._ID + "=" + tag.getId(),
                        null);
    }

    @Override
    public void delete(@NonNull Tag tag) {
        String deleteQuery = "DELETE FROM " + DatabaseDescriptor.TagEntity.TABLE_NAME + " WHERE " +
                DatabaseDescriptor.TagEntity.Column._ID + " = " + tag.getId() + ";";

        database.execSQL(deleteQuery);
    }
}
