package com.xinosluitsnoi.mymoney.domain.entity;

public class LinkTag {

    private final long linkId;

    private final long tagId;

    public LinkTag(long linkId, long tagId) {
        this.linkId = linkId;
        this.tagId = tagId;
    }

    public long getLinkId() {
        return linkId;
    }

    public long getTagId() {
        return tagId;
    }
}