package com.xinosluitsnoi.mymoney.domain.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor.LinkTagEntity;
import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor.LinkEntity;
import com.xinosluitsnoi.mymoney.domain.database.entity.LinkCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.database.entity.LinkTagCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.entity.Link;
import com.xinosluitsnoi.mymoney.domain.entity.LinkTag;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;
import com.xinosluitsnoi.mymoney.domain.mapper.LinkFromDBMapper;
import com.xinosluitsnoi.mymoney.domain.mapper.LinkTagFromDBMapper;
import com.xinosluitsnoi.mymoney.domain.mapper.LinkTagToDBMapper;
import com.xinosluitsnoi.mymoney.domain.mapper.LinkToDBMapper;
import com.xinosluitsnoi.mymoney.domain.repository.LinkRepository;
import com.xinosluitsnoi.mymoney.domain.repository.TagRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DBLinkRepository implements LinkRepository {

    @NonNull
    private SQLiteDatabase database;

    @NonNull
    private final TagRepository tagRepository;

    @NonNull
    private final LinkFromDBMapper linkFromDBMapper;

    @NonNull
    private final LinkToDBMapper linkToDBMapper;

    @NonNull
    private final LinkTagFromDBMapper linkTagFromDBMapper;

    @NonNull
    private final LinkTagToDBMapper linkTagToDBMapper;

    public DBLinkRepository(@NonNull DBHelper dbHelper,
                            @NonNull TagRepository tagRepository) {
        database = dbHelper.getReadableDatabase();
        this.tagRepository = tagRepository;

        linkFromDBMapper = new LinkFromDBMapper();
        linkToDBMapper = new LinkToDBMapper();
        linkTagFromDBMapper = new LinkTagFromDBMapper();
        linkTagToDBMapper = new LinkTagToDBMapper();
    }

    @NonNull
    @Override
    public List<Link> getAll() {
        List<Link> links = new ArrayList<>();
        String getAllLinksQuery = "SELECT * FROM " + LinkEntity.TABLE_NAME;

        try (Cursor cursor = database.rawQuery(getAllLinksQuery, null);
             LinkCursorWrapper linkCursorWrapper = new LinkCursorWrapper(cursor)) {
            if (linkCursorWrapper.moveToFirst()) {
                Link link;
                do {
                    link = linkFromDBMapper.map(linkCursorWrapper);
                    link.setTags(tagRepository.getAll(link));
                    links.add(link);

                } while (linkCursorWrapper.moveToNext());
            }
        }
        return links;
    }

    @NonNull
    @Override
    public List<Link> getAll(@NonNull Tag tag) {
        List<Link> links = new ArrayList<>();
        String getAllLinksQuery = "SELECT * FROM " + LinkTagEntity.TABLE_NAME +
                " WHERE " + LinkTagEntity.Column._ID + " = " + tag.getId();

        try (Cursor cursor = database.rawQuery(getAllLinksQuery, null);
             LinkTagCursorWrapper cursorWrapper = new LinkTagCursorWrapper(cursor)) {
            if (cursorWrapper.moveToFirst()) {
                LinkTag linkTag;
                do {
                    linkTag = linkTagFromDBMapper.map(cursorWrapper);
                    links.add(get(linkTag.getLinkId()));
                } while (cursorWrapper.moveToNext());
            }
        }
        return links;
    }

    @Nullable
    @Override
    public Link get(long id) {
        Link link;
        String getAllLinksQuery = "SELECT * FROM " + LinkEntity.TABLE_NAME;

        try (Cursor cursor = database.rawQuery(getAllLinksQuery, null);
             LinkCursorWrapper linkCursorWrapper = new LinkCursorWrapper(cursor)) {
            if (linkCursorWrapper.moveToFirst()) {
                    link = linkFromDBMapper.map(linkCursorWrapper);
                    link.setTags(tagRepository.getAll(link));
                    return link;
            } else {
                return null;
            }
        }
    }

    @Override
    public void add(@NonNull Link link) {
        ContentValues values = linkToDBMapper.map(link);
        database.insert(LinkEntity.TABLE_NAME, null, values);

        for (Tag tag : link.getTags()) {
            addTag(new LinkTag(link.getId(), tag.getId()));
        }
    }

    @Override
    public void update(@NonNull Link link) {
        ContentValues values = linkToDBMapper.map(link);
        database.update(LinkEntity.TABLE_NAME, values,
                        LinkEntity.Column._ID + " = " + link.getId(),
                        null);
    }

    @Override
    public void delete(@NonNull Link link) {
        String deleteQuery = "DELETE FROM " + LinkEntity.TABLE_NAME + " WHERE " +
                LinkEntity.Column._ID + " = " + link.getId() + ";";

        deleteTags(link);
        database.execSQL(deleteQuery);
    }

    @Override
    public void addTag(@NonNull LinkTag linkTag) {
        ContentValues values = linkTagToDBMapper.map(linkTag);
        database.insert(LinkTagEntity.TABLE_NAME,
                        null,
                        values);
    }

    @Override
    public void deleteTag(@NonNull LinkTag linkTag) {
        String deleteQuery = "DELETE FROM " + LinkTagEntity.TABLE_NAME + " WHERE " +
                LinkTagEntity.Column.TAGS_ID + " = " + linkTag.getTagId() + ";";

        database.execSQL(deleteQuery);
    }

    private void deleteTags(@NonNull Link link) {
        String deleteQuery = "DELETE FROM " + LinkTagEntity.TABLE_NAME + " WHERE " +
                LinkTagEntity.Column.LINKS_ID + " = " + link.getId() + ";";

        database.execSQL(deleteQuery);
    }
}