package com.xinosluitsnoi.mymoney.domain.repository;

import com.xinosluitsnoi.mymoney.domain.entity.Link;
import com.xinosluitsnoi.mymoney.domain.entity.LinkTag;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface LinkRepository {

    @NonNull
    List<Link> getAll();

    @NonNull
    List<Link> getAll(@NonNull Tag tag);

    @Nullable
    Link get(long  id);

    void add(@NonNull Link link);

    void update(@NonNull Link link);

    void delete(@NonNull Link link);

    void addTag(@NonNull LinkTag linkTag);

    void deleteTag(@NonNull LinkTag linkTag);
}