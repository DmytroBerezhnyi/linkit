package com.xinosluitsnoi.mymoney.domain.database;

import android.provider.*;

public interface DatabaseDescriptor {

    interface LinkEntity {

        String TABLE_NAME = "links";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                Column._ID + " INTEGER PRIMARY KEY NOT NULL," +
                Column.LINK + " TEXT NOT NULL," +
                Column.DESCRIPTION + " TEXT NOT NULL," +
                Column.IS_DONE + " INTEGER(1) NOT NULL" +
                Column.RATE + "INTEGER(4) NOT NULL);";

        interface Column extends BaseColumns {

            String LINK = "link";

            String DESCRIPTION = "description";

            String IS_DONE = "is_done";

            String RATE = "rate";
        }
    }

    interface TagEntity {

        String TABLE_NAME = "tags";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                Column._ID + " INTEGER PRIMARY KEY NOT NULL," +
                Column.TAG + " TEXT NOT NULL," +
                Column.RATE + "INTEGER(4) NOT NULL);";

        interface Column extends BaseColumns {

            String TAG = "tag";

            String RATE = "rate";
        }
    }

    interface LinkTagEntity {

        String TABLE_NAME = "linktag";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                Column._ID + " INTEGER PRIMARY KEY NOT NULL," +
                "FOREIGN KEY(" + Column.LINKS_ID + ") REFERENCES "
                + LinkEntity.TABLE_NAME + " (" + LinkEntity.Column._ID + ")," +
                "FOREIGN KEY(" + Column.TAGS_ID + ") REFERENCES "
                + TagEntity.TABLE_NAME + " (" + TagEntity.Column._ID + "));";

        interface Column extends BaseColumns {

            String LINKS_ID = "links_id";

            String TAGS_ID = "tags_id";
        }
    }
}