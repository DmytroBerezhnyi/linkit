package com.xinosluitsnoi.mymoney.domain.mapper;

import android.content.ContentValues;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;

import androidx.annotation.NonNull;

public class TagToDBMapper implements BaseMapper<ContentValues, Tag> {

    @NonNull
    @Override
    public ContentValues map(@NonNull Tag tag) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseDescriptor.TagEntity.Column.TAG, tag.getTag());
        contentValues.put(DatabaseDescriptor.TagEntity.Column.RATE, tag.getRate());

        return contentValues;
    }
}
