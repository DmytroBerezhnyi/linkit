package com.xinosluitsnoi.mymoney.domain.mapper;

import android.content.ContentValues;

import com.xinosluitsnoi.mymoney.domain.database.DatabaseDescriptor;
import com.xinosluitsnoi.mymoney.domain.entity.Link;

import androidx.annotation.NonNull;

public class LinkToDBMapper implements BaseMapper<ContentValues, Link> {

    @NonNull
    @Override
    public ContentValues map(@NonNull Link link) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseDescriptor.LinkEntity.Column.LINK, link.getLink());
        contentValues.put(DatabaseDescriptor.LinkEntity.Column.DESCRIPTION, link.getDescription());
        contentValues.put(DatabaseDescriptor.LinkEntity.Column.IS_DONE, link.isDone());
        contentValues.put(DatabaseDescriptor.LinkEntity.Column.RATE, link.getRate());

        return contentValues;
    }
}