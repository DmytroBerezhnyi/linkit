package com.xinosluitsnoi.mymoney.domain.database;
import android.database.sqlite.*;
import android.content.*;

public class DBHelper extends SQLiteOpenHelper
{

	private static final String DATABASE_NAME = "notes.db";
	
	private static final int DATABASE_VERSION = 1;
	
	
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DatabaseDescriptor.LinkEntity.CREATE_TABLE);
		database.execSQL(DatabaseDescriptor.TagEntity.CREATE_TABLE);
		database.execSQL(DatabaseDescriptor.LinkTagEntity.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase p1, int p2, int p3) {
		// none
	}
}