package com.xinosluitsnoi.mymoney.domain.mapper;

import com.xinosluitsnoi.mymoney.domain.database.entity.TagCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.entity.Tag;

import androidx.annotation.NonNull;

public class TagFromDBMapper implements BaseMapper<Tag, TagCursorWrapper> {

    @NonNull
    @Override
    public Tag map(@NonNull TagCursorWrapper cursorWrapper) {

        return new Tag(cursorWrapper.getId(),
                       cursorWrapper.getTag(),
                       cursorWrapper.getRate());
    }
}