package com.xinosluitsnoi.mymoney.domain.mapper;

import com.xinosluitsnoi.mymoney.domain.database.entity.LinkCursorWrapper;
import com.xinosluitsnoi.mymoney.domain.entity.Link;

import androidx.annotation.NonNull;

public class LinkFromDBMapper implements BaseMapper<Link, LinkCursorWrapper> {

    @NonNull
    @Override
    public Link map(@NonNull LinkCursorWrapper cursorWrapper) {
        return new Link(cursorWrapper.getId(),
                        cursorWrapper.getLink(),
                        null,
                        cursorWrapper.getDescription(),
                        cursorWrapper.getIsDone(),
                        cursorWrapper.getRate());
    }
}