package com.xinosluitsnoi.mymoney.domain.entity;

import androidx.annotation.NonNull;

public class Tag {

    private long id;

    private String tag;

    private int rate;

    public Tag(@NonNull String tag, int rate) {
        this.tag = tag;
        this.rate = rate;
    }

    public Tag(long id, @NonNull String tag, int rate) {
        this.id = id;
        this.tag = tag;
        this.rate = rate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getTag() {
        return tag;
    }

    public void setTag(@NonNull String tag) {
        this.tag = tag;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}