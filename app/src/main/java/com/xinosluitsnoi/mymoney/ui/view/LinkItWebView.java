package com.xinosluitsnoi.mymoney.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.CallSuper;

public class LinkItWebView extends WebView {

    public LinkItWebView(Context context) {
        super(context);
        init();
    }

    public LinkItWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkItWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        WebSettings webSettings = getSettings();

        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);

        setWebViewClient(new LinkItWebViewClient());
    }

    public static class LinkItWebViewClient extends WebViewClient {

        @TargetApi(Build.VERSION_CODES.N)
        @CallSuper
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        @CallSuper
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}