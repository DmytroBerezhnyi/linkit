package com.xinosluitsnoi.mymoney.ui.activity;

import android.os.*;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.xinosluitsnoi.mymoney.R;
import com.xinosluitsnoi.mymoney.mvp.contract.MainView;
import com.xinosluitsnoi.mymoney.mvp.presenter.MainPresenter;
import com.xinosluitsnoi.mymoney.ui.fragment.BrowserFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    @InjectPresenter
    MainPresenter mainPresenter;

    private BrowserFragment browserFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "BrowserFragment", browserFragment);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void init(Bundle savedInstanceState) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            firstCreatedFragment(fragmentManager);
        } else {
            recreatedFragment(savedInstanceState, fragmentManager);
        }
    }

    private void firstCreatedFragment(@NonNull FragmentManager fragmentManager) {
        browserFragment = new BrowserFragment();
        int container = R.id.container_view;

        fragmentManager.beginTransaction()
                       .add(container, browserFragment)
                       .commit();
    }

    private void recreatedFragment(@NonNull Bundle savedInstanceState,
                                   @NonNull FragmentManager fragmentManager) {
        browserFragment = (BrowserFragment) fragmentManager.getFragment(
                savedInstanceState,
                "BrowserFragment");
    }
}