package com.xinosluitsnoi.mymoney.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.xinosluitsnoi.mymoney.mvp.contract.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    public MainPresenter() {
        getViewState().showMessage("HEyo");
    }
}