package com.xinosluitsnoi.mymoney.mvp.contract;

import com.arellomobile.mvp.MvpView;

public interface MainView extends MvpView {
    void showMessage(String message);
}
