package com.xinosluitsnoi.mymoney.mvp.contract;

public interface MainRouter {

    void showLinksScreen();

    void showTagsScreen();

    void showSettingsScreen();
}